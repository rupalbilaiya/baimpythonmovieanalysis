
# Determination of  factors that impact the success of a movie



![Logo](https://nollytech.com/wp-content/uploads/2020/05/NETFLIX-VS-AMAZON-PRIME.jpg)

There’s a joke in Hollywood where they say, “How do you market a movie?” And all the studio executives get on a rooftop and scream, “Come see my movie!”. However, this is no longer the trend. Movie industry and associated tech-giants who run movie-business models want to explore data to emerge into new markets, amplify sales and penetrate the market. 
The entertainment industry is an inherently costly endeavor and capturing the attention and hard-earned money of consumers requires a significant financial commitment. Spending more than your competitors doesn’t necessarily guarantee victory. But it certainly isn’t an impediment to progress. The budgets allocated by Netflix and Amazon for purchasing license rights for movies are mentioned below. 
Netflix: $17 Billion, Amazon Prime Video: $9 Billion
As business analysts, we aim to cite recommendations and proposals based on data to tech-giants regarding the factors to be considered by content platforms/production companies prior to acquiring license for a specific content for them to maximize profitability.


## Authors

- [@Rupal Bilaiya](rupal.bilaiya@gmail.com)


## Acknowledgements

 - Dataset used (https://www.kaggle.com/rounakbanik/the-movies-dataset/metadata)
 - https://www.theatlantic.com/sponsored/ibm-transformation-of-business/big-data-and-hollywood-a-love-story/277/
 - https://observer.com/2021/05/amazon-apple-netflix-disney-viacomcbs-nbcu-content-budgets/


## Tech Stack

**Language:** Python

**Tools:** Pycharm, Jupyter Notebook


## Code Description

- Data Cleaning
Scrapped missing information from imdb website using BeautifulSoup

Using literal eval converted strings into dictionary

Made data consistent

- EDA
As part of EDA, explored relation between rating column with corresponding factors provided. Analysed factors like released date, duration, movie span, popularity, revenue and many more.

Results are present in the presentation attached in GITLAB.

- Model
After performing basic OLS model on the provided data, following were my interpretations:
    
    - Runtime plays an important part and should be considered while acquiring rights for a movie.
    - When in doubt, go for “DRAMA”!
    - Top movies from other non-English speaking nations do add value to the business.

#### Future scope:
- Inclusion of factors like cast and director
- Predicting a list of most profitable movies
- Predicting if a given movie is likely to be lucrative before a theatrical release


## Documentation

[GITLAB Project]()

